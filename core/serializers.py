from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from core.models import Ingredient, Tag


class TagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class IngredientSerializer(ModelSerializer):
    tags = TagSerializer(many=True)

    class Meta:
        model = Ingredient
        exclude = ('created_by',)


class IngredientCreateTagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id',)


class IngredientCreateSerializer(ModelSerializer):
    tags = serializers.ListField(child=serializers.IntegerField(), required=False)
    bases = serializers.ListField(child=serializers.IntegerField(), required=False)
    ingredient_type = serializers.ChoiceField(choices=Ingredient.IngredientType.choices, required=False)

    class Meta:
        model = Ingredient
        exclude = ('created_by',)

    def is_valid(self, *, raise_exception=False):
        try:
            self.initial_data['ingredient_type'] = self.initial_data['ingredient_type'].value
        except AttributeError:
            pass
        except KeyError:
            pass
        return super().is_valid(raise_exception=raise_exception)
