class ExceptionMiddleware(object):

    def on_error(self, error):
        try:
            raise error['detail']
        except KeyError:
            pass
        raise error

    def resolve(self, next, root, info, **args):
        return next(root, info, **args)
