import graphene
import graphene_django
from django.contrib.auth import get_user_model
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.rest_framework.mutation import SerializerMutation
from rest_framework_simplejwt.exceptions import AuthenticationFailed

from core import models
from core.serializers import IngredientCreateSerializer


class UserNode(graphene_django.DjangoObjectType):
    class Meta:
        model = get_user_model()
        interfaces = (graphene.relay.Node,)
        filter_fields = ['username']
        exclude = ('password', 'last_login', 'email', 'date_joined')


class IngredientNode(graphene_django.DjangoObjectType):
    class Meta:
        model = models.Ingredient
        filter_fields = {
            'name': ['exact'],
            'ingredient_type': ['exact'],
            'tags': ['exact', 'in']
        }
        interfaces = (graphene.relay.Node,)


class IngredientMutation(SerializerMutation):
    class Meta:
        serializer_class = IngredientCreateSerializer

    @classmethod
    def perform_mutate(cls, serializer, info):
        serializer.validated_data['created_by'] = info.context.user
        return super().perform_mutate(serializer, info)

    @classmethod
    def mutate(cls, root, info, input):
        if not info.context.user.is_authenticated:
            raise AuthenticationFailed
        return super().mutate(root, info, input)


class TagNode(graphene_django.DjangoObjectType):
    class Meta:
        model = models.Tag
        filter_fields = ['name', 'ingredients']
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    ingredient = graphene.relay.Node.Field(IngredientNode)
    all_ingredients = DjangoFilterConnectionField(IngredientNode)

    tag = graphene.relay.Node.Field(TagNode)
    all_tags = graphene_django.filter.DjangoFilterConnectionField(TagNode)

    user = graphene.relay.Node.Field(UserNode)
    all_users = graphene_django.filter.DjangoFilterConnectionField(UserNode)


class Mutation(graphene.ObjectType):
    ingredient_mutation = IngredientMutation.Field()
