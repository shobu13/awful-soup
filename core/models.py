from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.functions import Lower
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    pass


class Ingredient(models.Model):
    """
    A ingredient that can be a base or and ingredient.
    A base ingredient is the base word you search descriptive for or alts

    Base don't have tag and ingredients can have multiple adjective tags
    """

    class IngredientType(models.IntegerChoices):
        BASE = 0, _('Base')
        INGREDIENT = 1, _('Ingredient')

    name = models.CharField(max_length=100)
    ingredient_type = models.IntegerField(choices=IngredientType.choices)
    tags = models.ManyToManyField(to='core.Tag', related_name='ingredients')
    alt = models.ForeignKey(to='core.Ingredient', blank=True, null=True, related_name='alts', on_delete=models.CASCADE)
    bases = models.ManyToManyField(to='core.Ingredient', blank=True, related_name='ingredients')
    definition = models.TextField()
    examples = models.TextField()  # examples are separate with a §

    created_by = models.ForeignKey(to=get_user_model(), on_delete=models.SET(1))

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name'], name='unique_name'),
        ]

    def __str__(self) -> str:
        return self.IngredientType(
            self.ingredient_type).label + ': ' + self.name + (f" > {self.alt.name}" if self.alt else '')


class Tag(models.Model):
    """
    A tag represent a qualitative of an ingredient.

    A tag can have multiple ingredients, and an ingredient can have multiple tags.
    """

    name = models.CharField(max_length=100)
    color = models.CharField(max_length=100, blank=True, choices=((i, i) for i in (
        'red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green',
        'lime', 'yellow', 'amber', 'orange', 'deep-orange', 'brown', 'blue-grey', 'grey')))

    def __str__(self) -> str:
        return self.name
