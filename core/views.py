from graphene_django.views import GraphQLView
from rest_framework import request as rq
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings


class DRFAuthenticateGraphQLView(GraphQLView):
    def parse_body(self, request):
        if isinstance(request, rq.Request):
            return request.data
        return super(DRFAuthenticateGraphQLView, self).parse_body(request)

    @classmethod
    def as_view(cls, *args, **kwargs):
        view = super().as_view(*args, **kwargs)
        view = permission_classes(api_settings.DEFAULT_PERMISSION_CLASSES)(view)
        view = authentication_classes(api_settings.DEFAULT_AUTHENTICATION_CLASSES)(view)
        view = api_view(['GET', 'POST'])(view)
        return view
